# Helm chart for loki logging stack

This chart will install [loki stack](https://grafana.com/oss/loki/)(loki, grafana, promtail) on kubernetes cluster.

## Installation

1. Prepare NFS server

2. Clone repo and run

``` bash
helm dependancy update
helm upgrade --install --namespace logging --create-namespace loki-stack .
kubectl get secret --namespace logging loki-stack -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

## Problems

### Grafana with NFS

Problem:
grafana pod unvaible, init container crash
Reason:
NFS use root_squash by default, so container mount that volume can't have root access, so can't use chown.
Grafana default use init container to chown => crash at init
Fix:
Enabled no_root_squash in NFS server OR include this

``` yaml
# values.yaml
grafana:
  initChownData:
    enabled: false
```

### Inconsistent PVC

Problem: helm delete will delete grafana pvc but not loki pvc
Fix: delete loki pvc manually after helm delete

### Release name and values.yaml

Problem: Using another release name will cause problem without changing value yaml content
Reason: Can't use variable in values.yaml. Some key in values.yaml depend on Release.Name
